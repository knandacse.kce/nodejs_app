# node-chat-app

Nodejs Chat Application 
### Installation

Follow the instructions below to run the app

```sh
$ git clone https://gitlab.com/knandacse.kce/nodejs_app.git
$ cd node-chat-app
$ npm install -d
$ npm start
```
Visit http://localhost:3001 in your browser to view the app.

### Test
Test the application 
```sh
$ npm test
```
For continous testing
```sh
$ npm run test-watch
```
